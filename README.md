# Devscola Tree

## Development

Requisites: `docker` & `docker-compose`

### Up & running

Run: `docker-compose up --build`. 

APP is accesible via browser [localhost:3000](http://localhost:3000) and API is listening on `3001`. You can run this command: `curl -X POST http://localhost:3001/`

## Backend
Deploy to heroku: `git subtree push --prefix path/to/subdirectory heroku master`
https://devscola-tree-api.herokuapp.com/

## Frontend
Deploy pushing to master branch
https://devscola-tree-frontend.netlify.com/