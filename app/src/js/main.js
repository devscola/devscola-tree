import TreeService from "./TreeService.js";
import TreeGraph from "./TreeGraph.js";

const treeService = new TreeService();

fetch('/data/tree.json').
  then((response) => response.json()).
  then((relations) => {
    let treeGraph

    relations["data"].forEach(relation => {
      treeGraph = treeService.add(relation.child, relation.parent)
    });
    
    // how to avoid dirtying the global namespace?
    const $addChildButton = document.querySelector("#newChild-add"); 
    const $childInput = document.querySelector("#newChild-input")
    const $parent = document.querySelector("#parent")
    const $overlay = document.querySelector("#modal .overlay");
    const $modal = document.querySelector("#modal");
    
    const showModal = parent => {
      $parent.innerHTML = parent;
      $modal.classList.remove("hidden");
      $childInput.focus()
    };
    
    const closeModal = () => {
      const childInput = $childInput;
      childInput.value = ''
      $modal.classList.add("hidden");
    }
    
    const updateGraph = () => {
      const child = $childInput.value;
      const parent = $parent.innerHTML
      
      treeGraph = treeService.add(child, parent)
      graph.update(treeGraph[0])
    }
    
    $overlay.addEventListener("click", () => {
      closeModal()
    });
    
    $childInput.addEventListener("keypress", ({code}) => {
      if(code === 'Enter'){
        updateGraph()
        closeModal()
      }
    });
    
    $addChildButton.addEventListener("click", () => {
      updateGraph()
      closeModal()
      
    });
    
    /// Tree Painting
    const data = treeGraph[0];
    const graph = new TreeGraph(data, "#tree", showModal); // How to avoid passing the function here?
    
    graph.draw();
  })