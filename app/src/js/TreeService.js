class TreeService {
  constructor() {
    this.tree = [];
  }

  createNode(name, children = []) {
    return {
      name,
      children
    };
  }

  findInTree(name, nodes) {
    if (!nodes) return undefined;

    let node = nodes.find(node => node.name === name);

    if (!node) {
      for (let i = 0; i < nodes.length; i++) {
        let newLevel = nodes[i];

        node = this.findInTree(name, newLevel.children);

        if (node) return node;
      }
    }

    return node;
  }

  findOrCreateNode(parent) {
    let node = this.findInTree(parent, this.tree);
    if (node) return node;

    node = this.createNode(parent);
    this.tree.push(node);

    return node;
  }

  add(child, parent) {
    const node = this.findOrCreateNode(parent);
    const children = this.createNode(child);

    node.children.push(children);
    return this.tree;
  }
}

// module.exports = TreeService
export default TreeService;
