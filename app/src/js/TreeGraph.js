function svgBoundingBox() {
  const { x, y, width, height } = this.getBBox(); // Get current dimensions of svg
  return [x, y, width, height];
}

class TreeGraph {
  constructor(data, selector, handleNodeClick = () => {}) {
    this.data = data;
    this.selector = selector;
    this.handleNodeClick = handleNodeClick;
    this._buildTree();
  }

  _buildTree() {
    const width = 932;
    const radius = width / 2;
    const degrees360inRadians = 2 * Math.PI;

    const tree = d3 // Setups the tree entity graph
      .tree()
      .size([degrees360inRadians, radius])
      .separation((a, b) => (a.parent == b.parent ? 1 : 2) / a.depth); // Separation accesor for radial graph https://github.com/d3/d3-hierarchy/blob/master/README.md#tree_separation

    const treeData = d3
      .hierarchy(this.data) // Generates hierarchy object needed to paint the tree
      .sort((a, b) => d3.ascending(a.data.name, b.data.name)); // Ordenar alfabéticamente los nodos de cada capa

    this.tree = tree(treeData);
  }

  _appendSVG() {
    this.svg = d3.select(this.selector).append("svg");
  }

  _drawLinks() {
    const linkData = this.tree.links();

    const drawCurve = () =>
      d3
        .linkRadial() // Draw bezier curve between two points
        .angle(data => data.x)
        .radius(data => data.y);

    this.svg
      .append("g")
      .selectAll("path")
      .data(linkData)
      .join("path")
      .attr("class", "tree__link")
      .attr("d", drawCurve());
  }

  _drawNodes() {
    const nodeData = this.tree.descendants().reverse();

    this.nodes = this.svg
      .append("g")
      .selectAll("g")
      .data(nodeData)
      .join("g")
      .attr("class", "tree__node")
      .attr(
        "transform",
        data => `
      rotate(${(data.x * 180) / Math.PI - 90})
      translate(${data.y},0)
    `
      );
  }

  _drawPoints() {
    const getNodeColor = isTerminal => (isTerminal ? "tree__node--terminal" : '');

    this.nodes
      .append("circle")
      .attr('class', data => getNodeColor(!data.children))
      .attr("r", 2.5);
  }

  _drawLabels() {

    const isLabelRightToNode = data => {
      const isTerminalLabel = !data.children
      const angle = data.x
      const degrees180InRadians = Math.PI;

      const isLabelRotatedLessThan180Degrees = angle < degrees180InRadians
      return isLabelRotatedLessThan180Degrees === isTerminalLabel
    }

    const isTextUpsideDown = data => {
      const degrees180InRadians = Math.PI;
      const angle = data.x

      return angle >= degrees180InRadians
    }

    this.nodes
      .append("text")
      .attr("class", "tree__label")
      .attr("dy", "5px") // Offset text, half is height to align it to center of the line (local coords)
      .attr("x", (data) => isLabelRightToNode(data) ? 6 : -6)
      .attr("text-anchor", (data) => isLabelRightToNode(data) ? "start" : "end")
      .attr("transform", (data) => isTextUpsideDown(data) ? "rotate(180)" : null)
      .text(({data}) => data.name)
      .clone(true)
      .lower()
  }

  _addNodeClickListeners() {
    this.nodes
      .selectAll("text")
      .on("click", ({ data }) => this.handleNodeClick(data.name));
  }

  _setViewBox() {
    this.svg.attr("viewBox", svgBoundingBox);
  }

  _cleanSVG(){
    this.svg.selectAll("*").remove();
  }

  update(data){
    this.data = data
    this._cleanSVG()
    this._buildTree()
    this._drawNodes();
    this._drawLinks();
    this._drawPoints();
    this._drawLabels();
    this._addNodeClickListeners();
    this._setViewBox();
  }

  draw() {
    this._appendSVG()
    this._drawNodes();
    this._drawLinks();
    this._drawLabels();
    this._drawPoints();
    this._addNodeClickListeners();
    this._setViewBox();
  }
}

export default TreeGraph;
