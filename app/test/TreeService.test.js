import TreeService from "../src/js/TreeService";

describe("TreeService", () => {
  it("adds a leaf", () => {
    const service = new TreeService();
    const expectedLeaf = {
      name: "bianca",
      children: [
        {
          name: "alvaro",
          children: []
        }
      ]
    };

    const tree = service.add("alvaro", "bianca");

    expect(tree).toContainEqual(expectedLeaf);
  });

  it("adds some leaves", () => {
    const service = new TreeService();
    const expectedOneLeaf = {
      name: "bianca",
      children: [
        {
          name: "alvaro",
          children: []
        }
      ]
    };
    const expectedTwoLeaf = {
      name: "lolo",
      children: [
        {
          name: "rosa",
          children: []
        }
      ]
    };

    service.add("alvaro", "bianca");
    const tree = service.add("rosa", "lolo");

    expect(tree).toContainEqual(expectedOneLeaf);
    expect(tree).toContainEqual(expectedTwoLeaf);
  });

  it("adds another leaf to an existing parent", () => {
    const service = new TreeService();
    const expectedBranch = {
      name: "victor",
      children: [
        {
          name: "miguel",
          children: []
        },
        {
          name: "rosa",
          children: []
        }
      ]
    };

    service.add("miguel", "victor");
    const tree = service.add("rosa", "victor");

    expect(tree).toContainEqual(expectedBranch);
  });

  fit("adds a children to a second level parent", () => {
    const service = new TreeService();

    const expectedTree = [{
      name: "victor",
      children: [
        {
          name: "miguel",
          children: [
            {
              name: "samuel",
              children: []
            }
          ]
        }
      ]
    }];

    service.add("miguel", "victor");
    const tree = service.add("samuel", "miguel");

    expect(tree).toEqual(expectedTree) 
  });
});
